# Built-in Functions

## Capitalize strings

**be aware of the `capitalize()` side effects!**

Python's built-in `capitalize()` function turns the first character of a string in uppercase:

```python
string = "hallo, welt!"
print(string.capitalize())
# Hallo, welt!
```

However, the `capitalize` not just capitalizes the first character, but also turns everything else into **lowercase**:

```python
string = "hallo, Welt!"
print(string.capitalize())
# Hallo, welt!
```

Unfortunately, there doesn't exist a function like `ucfirst` which would leave the rest of the string untouched. Instead, we have to do it manually:

<strong>

```python
string = "hallo, Welt!"
print(string[:1].upper() + string[1:])
# Hallo, Welt!
```
</strong>

## Split strings

Splitting a string by specifying a delimiter is very easy:

```python
parts = 'a sentence with some words'.split(' ')
# parts = ['a', 'sentence', 'with', 'some', 'words']
```

However, when you are expecting a given number of values (e.g. firstname and lastname) and want to directly assign them to variables, you'll run into ValueError:

```python
firstname, lastname= 'Voldemort'.split(' ')

ValueError: not enough values to unpack (expected 2, got 1)
```

How can you assure one always gets enough values and if the lastname does not exist, it will receive a default value (e.g. an empty string)?

<strong>

```python
firstname, lastname, *_ = 'Voldemort'.split(' ') + ['']   # lastname is ''
firstname, lastname, *_ = 'Dalai Lama'.split(' ') + ['']  # lastname is 'Lama'
```
</strong>

The trick is to ensure that the right side of the assignment always returns at least 2 elements. If more elements are returned, they will be passed to the magic underscore `_` variable. The `*_` means that this magic variable is treated as an array. [This page](https://www.datacamp.com/community/tutorials/role-underscore-python) shows more tricks you can do by using this variable.

**`re.split`: more sophisticated split using regular expressions**

In certain cases, you want to split your string using alternatives. For example you would like to extract scheme from a URL, allowing some common typos:

* `https://domain.org`
* `https:/mistake.org`
* `https:///anothermistake.org`
* `https:idonotfindtheslash.org`

To still be able and split the scheme from the domain name, you need to use the split method from the `re` module.


```python
import re
scheme, url = re.split(r":///|://|:/|:", "http:/mistake.org")
```

## Filtering: the `filter` function

**input:** a file list which needs to be filtered (and later sorted):

```
20_Ms_229_7.xml
20_Ms_229_37.xml
20_Ms_229_6.xml
20_Ms_229_29.xml
20_Ms_229_15.xml
229.xpr
20_Ms_229_17.xml
20_Ms_229_4.xml
20_Ms_229_5.xml
20_Ms_229_16.xml
semper_edition_schema_prov.rng
schema_semper_with_mathml.rng
20_Ms_229_38_verso.xml
...
```

**create the filter function**

* files which do not start with a number should be filtered out
* i.e. the file should match the regular expression `^\d+`
* if the match is successful, return a True or true-like value
* since a non-match corresponds to a False, we can just return the match itself:

```python 
import re

def my_filter(val):
    match = re.search(r'^\d+', val)
    return match
```

**Apply the filter function**

* the `filter` function takes two arguments:
* our defined `my_filter` function
* the list of files
* this is called **functional programming**

```python
import os
selected_files = []
for root, dirs, files in os.walk('.'):
    selected_files += filter(my_filter, files)
```

## Sorting: the `sorted` function

**Input** the list of files, but this time we would like to apply a two-dimensional sort:

1. sort by the first two digits, e.g. `20` in  `20_Ms_229_7.xml`
2. sort by the last digit, e.g. `15` in `20_Ms_229_15.xml` in descending order

**define the sort functions**

```python
import re

def sort_by_first_number(filename):
    match = re.search(r'^(\d+)', filename)
    if match:
        return int(match.groups()[0])
    
def sort_by_last_number(filename):
    match = re.search(r'_(\d+)\.xml', filename)
    if match:
        return int(match.groups()[0])
```

**Note:** when doing string comparison, we just return the string (or `string.lower()` for case-insensitive sort). Because we want to compare numbers, we need to apply the `int()` function to enforce number comparison.


**apply the sort functions using `sorted`**

```python
sorted_filenames = sorted(
    sorted(
        filenames, 
        key=sort_by_last_number, 
        reverse=True
    ),
    key=sort_by_first_number
)

# sorted_filenames
['10_Ms_229_29.xml',
 '10_Ms_229_15.xml',
 '20_Ms_229_37.xml',
 '20_Ms_229_7.xml',
 '20_Ms_229_6.xml',
 ...
 ]
```




