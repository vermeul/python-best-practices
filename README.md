# Python Best Practices

This guide shows you all the pitfalls to avoid and modules you should use (or shouldn't). Be aware: advices should not be followed blindly. If you do not agree to an advice, this is completely fine. The important step is: make your own decision and then stick to it.

Useful websites that help you out:

* **Python 3 Module of the Week** All standard Python modules nicely explained. https://pymotw.com/3/
* **The Hitchhiker’s Guide to Python!** Many very useful modules explained which are not part of the standard Python module collection. https://docs.python-guide.org
* **PyPi** The Python Package Index which stores (almost) all external modules and where you might want to publish your own. It is the source of the installed modules whenever you do a `pip install XXX`. https://pypi.org
* A collection of **useful Python tips:** http://book.pythontips.com/en/latest/index.html

Some websites and talks about «good Python coding»:

* Python Anti-Patterns: https://docs.quantifiedcode.com/python-anti-patterns/
* Google Best Practices with Python: https://github.com/google/styleguide/blob/gh-pages/pyguide.md
* [Transforming Code into Beautiful, Idiomatic Python -- Raymond Hettinger](https://www.youtube.com/watch?v=OSGv2VnC0go)
* [Clean code in Python -- Mariano Anaya](https://www.youtube.com/watch?v=7ADbOHW1dTA)
