# Values and Expressions

## Long strings

Code should not become longer than about 120 characters, but strings often get much longer. Use round brackets `( )` on mutltiple strings to create a long string. Use the [K&R style](https://en.wikipedia.org/wiki/Indentation_style#K&R_style) (opening bracket must stay on first line!) for improved readability:

<strong>

```python
my_long_company = (
    "Donau"
    "dampfschifffahrts"
    "elektrizitäten"
    "hauptbetriebswerk"
    "bauunterbeamten"
    "gesellschaft"
)
# will be magically concatenated to Donaudampfschifffahrtselektrizitätenhauptbetriebswerkbauunterbeamtengesellschaft 
```
</strong>

Unfortunately, you cannot do this, as it will lead to a syntax error:

```python
my_long_company = (
    "Donau"
    "dampfschifffahrts"
    "elektrizitäten"
    ", ".join(["something","completely", "different"])
    "hauptbetriebswerk"
    "bauunterbeamten"
    "gesellschaft"
)
    "Donau"
    ^
SyntaxError: invalid syntax. Perhaps you forgot a comma?
```

You need to use **+++ instead:

<strong>

```python
my_long_company = (
    "Donau"
    +"dampfschifffahrts"
    +"elektrizitäten"
    +", ".join(["something","completely", "different"])
    +"hauptbetriebswerk"
    +"bauunterbeamten"
    +"gesellschaft"
)
```
</strong>




## Multiline strings

If a string has embedded newline characters, you might define it this way:

```python
lines = "first line\n"\
        "second line\n"\
        "third line"
```
However, putting all the backslashes at the end of each line makes the code noisy. Here is a better way, which also uses [K&R style](https://en.wikipedia.org/wiki/Indentation_style#K&R_style):

<strong>

```python
lines = (
    "first line\n"
    "second line\n"
    "third line"
)
```
</strong>
You still need to include the `\n` at the end of each line, though.

Another way is to use triple apostrophes `"""`, also called **docstrings**:

```python
lines = """
first line
second line
third line
"""
```
Unfortunately, the example above will prepend a linebreak. To avoid that, put a backslash ``\`` at the end of the first line:

<strong>

```python
lines = """\
first line
second line
third line
"""
```
</strong>

Be aware that empty spaces are introduced when you use indentation for better readability:

```python
def get_text():
    lines = """\
        first line
        second line
        third line
    """
    return lines

print(get_text())
```
will actually print this:

```
        first line
        second line
        third line
```
So you need to do this instead:

<strong>

```python
def get_text():
    lines = """\
first line
second line
third line
    """
    return lines

print(get_text())
```
</strong>
This will unfortunately break the indentation but is the least amount of typing.

## Long Numbers

For better readability, you can use the underscore **_** to define long numbers:

```python
population_worldwide = 8_006_854_365
```
