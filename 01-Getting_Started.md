# Getting Started

## Which Python version should I choose?

There is still **Python 2.x** around (usually 2.5 or 2.7) and there is **Python 3.x** (currently 3.12). Both languages are very similar, but not compatible to each other. Python 2 is no longer supported since April 2020. If you start a new project, start with a recent version of Python 3. While **Python 2.7** is no longer maintained, every once in a while you might still encounter an old Python 2.7 project. Use a [Python2 to Python3 converter](https://docs.python.org/2/library/2to3.html) to convert such a project into valid Python3 code.

Get a fresh Python here:

- Official release: https://www.python.org/downloads/
- Miniconda release: https://conda.io/miniconda.html
- Anaconda release: https://www.anaconda.com/download/#macos

**Even better method**: use [pyenv](https://github.com/pyenv/pyenv) to be able to install different Python versions side-by-side without ending up in a mess (described in more detail below)


## My OS already speaks Python

**Do not use the operating systems’ Python**

The python from your operating system (most likely Python 2.7.x) is usually not the version you want to work with. Consider Python 2.7 being a different language than Python 3, and it is highly recommended that you start new projects in Python 3.6 or later (at least Python 3.4). The system needs Python 2.7 for its own purposes, so an update of the libraries (pip install --upgrade) might also change the behaviour of your system!

## Know your Python

### pyenv to install any Python version

There are way too many ways to install Python, that's why many developers run into an installation mess. **pyenv** is an utility that helps you easily install any Python version you like (from any vendor). Switching between different Python versions becomes extremely easy and happens automatically when you `cd` into a project that uses a specific Python version.

[https://github.com/pyenv/pyenv](https://github.com/pyenv/pyenv)

```
$ git clone https://github.com/pyenv/pyenv.git ~/.pyenv
$ echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
$ echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile
$ echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bash_profile
$ exec "$SHELL"
```

### pyenv for Windows

Luckily, pyenv has been [ported to Windows](https://github.com/pyenv-win/pyenv-win)! Installation is quite straight forward:

0. install [chocolately](https://chocolatey.org), the package manager for Windows

- search for `PowerShell`
- user right-click to **Run as administrator**
- paste this to install

```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

- or follow the [installation instructions on their website](https://chocolatey.org/install#individual)

1. install pyenv-win

- open PowerShell as admin (see above)
- run `choco install pyenv-win`

2. run pyenv

- open a **cmd** shell
- search for `cmd`
- execute `pyenv` to check that it is properly installed

```
$ pyenv install --list
Available versions:
  2.1.3
  2.2.3
  ... (very long list)
  3.6.0
  3.6-dev
  3.7-dev
  ... (continuing)
  stackless-3.4.1
  stackless-3.4.2

$ pyenv install miniconda3-latest
```

List all the Python versions currently installed in your system:

```
$ pyenv versions
  system
  2.7.13
  3.3.6
  3.4.4
  3.5.3
* 3.6.9 (set by /Users/vermeul/.python-version)
  miniconda3-latest
  pypy-5.6.0
```

Locally (only current directory and subdirectories) switch to Miniconda-Python:

```
$ cd tmp
$ pyenv local miniconda3-latest
$ pyenv version
miniconda3-latest (set by /Users/vermeul/tmp/.python-version)
```

### Install Python under Windows with _no admin privileges_

If you happen to have a fully managed machine with no Windows Subsystem for Linux (WSL) installed, things get more complicated - unless you would like to use Microsoft Store. Here are some instructions to get it working without the Microsoft Store and without admin privileges:

1. go to https://www.python.org/downloads
2. go to the **specific releases** section and e.g. [Python 3.11](https://www.python.org/downloads/release/python-3114/)
3. download the [Windows embeddable package (64-bit)](https://www.python.org/ftp/python/3.11.4/python-3.11.4-embed-amd64.zip)
4. extract the `.zip` file and move the folder to a convenient place, e.g. you Desktop
5. go to the Windows System Properties and edit the **Environment Variables**
6. add/edit the User variable `Path` (replace `username` with your own username)
   - `C:\Users\username\Desktop\python-3.11.4-embed-amd64`
   - `C:\Users\username\Desktop\python-3.11.4-embed-amd64\Scripts`
7. move the `%USERPROFILE%\AppData\Local\Microsoft\WindowsApps` entry down, below the entries you added above.
8. if you now open a new PowerShell window, you should be able to execute `python`. Hit `CTRL-Z` and `Enter` to leave the interpreter
9. Unfortunately, Python for Windows does not recognise the usual `PYTHONPATH` variable to find additionally installed packages. Instead, it reads this path information from a file. Inside the folder with the Python executable, look for a file named `python311._pth` and add the following line:
   - `Lib/site-packages`
   - (this folder contains the additonally installed packages, such as pip)
10. Install `pip`, the Python Package Manager:

- download https://bootstrap.pypa.io/get-pip.py and make sure to save the script as `get-pip.py`
- in the PowerShell terminal, execute it: `python get-pip.py`

11. now `pip` should be working as expected

## Troubleshooting SSH/TLS

If you are having trouble to use pip or installing a new Python version, you might need to upgrade openSSL and then reinstall the Python version:

```
brew install 'openssl@1.1'
CONFIGURE_OPTS="--with-openssl=$(brew --prefix openssl)" pyenv install 3.7.0
```

## Know your environment!

**use `virtualenv` to separate the modules used for your project from your global installation**

Virtualenv is a tool which allows us to make isolated Python environments. Imagine you have an application that needs version 2 of a library, but another application requires version 3. How can you use and develop both these applications? Here is where virtualenv comes into play:

```
$ pip install virtualenv
```

The most important commands are:

```
$ virtualenv myproject
Using base prefix '/Users/vermeul/.pyenv/versions/3.6.0'
New python executable in /Users/vermeul/tmp/myproject/bin/python3.6
Also creating executable in /Users/vermeul/tmp/myproject/bin/python
Installing setuptools, pip, wheel...done.
$ source myproject/bin/activate
(myproject) $ pip install 'MyModule>=3.0'
(myproject) $ deactivate
```

This will:

- set up a project `myproject`
- create links to your existing Python installation
- ... but without the modules
- `pip install` will install the module only locally
- `deactivate` leaves the project environment

## Playing is the best way to learn!

**Use _IPython_, _Jupyter_ and _Jupyter Lab_ to play with the language**

Jupyter notebooks are becoming the most important tool for data scientists to combine text, code and graphics in one document, all running in a simple browser window. You do not need any editor to create Python files, just enter code in a cell and execute it!

Jupyter (formerly known as IPython) is the predecessor of Jupyter Lab. The both share the same notebook-format, which is a JSON-file with the `.ipynb` ending. IPython is a highly recommended REPL (Read, Evaluate, Print-Loop) for the command line. It is the so called _Python kernel_ used in Jupyter.

- [IPython documentation](http://ipython.org)
- [Jupyter Documentation](https://jupyter.org)
- Install Jupyter Notebook:[https://jupyter.org/install.html](https://jupyter.org/install.html)
- [Jupyter Lab Documentation](https://jupyterlab.readthedocs.io/en/stable/index.html)
- Install Jupyter Lab: [https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html](https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html)
