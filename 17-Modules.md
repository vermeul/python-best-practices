# Modules and Packages

The best documentation on how to set up a module so it can be published as a Python package can be found on the Python Package Index (PyPi) website: https://packaging.python.org/tutorials/packaging-projects/


## Include documentation

**include your README.md in your setup.py**

Every project should include a `README.md` file which describes how the module should be used. The Python Package Index (PyPi) will render the content on its website if you include it in the `long_description` parameter. Do not forget the encoding and set it to `utf-8`:

<strong>

```python
with open("README.md", "r", encoding="utf-8") as fh: 
    long_description = fh.read() 

setup(
    name='my_famous_first_module',
    description='A module which makes everything easier.',
    long_description=long_description,
)
```
</strong>


## Version requirements

**Enforce your version requirements programmatically**

Since the advent of Python 3.0, many features have been added to the language (e.g. easy merging
of two dictionaries). If you release a Module on PyPi (Python Package Index), you will put this
information into `setup.py`:

<strong>

```python
setup(
    python_requires=">=3.5"
    ...
)
```
</strong>

But often enough, you are just writing a script. In any case, it is wise not just to declare it
but enforce the version requirement programmmatically:

<strong>

```python
use sys
assert sys.version_info >= (3, 5)   # only works under Python version 3.5 or later
```
</strong>
