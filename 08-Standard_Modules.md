[Python 3 Module of the Week](https://pymotw.com/3/) (PyMOTW-3) is a series of articles written by Doug Hellmann to demonstrate how to use the modules of the Python 3 standard library. This means, those modules can be used just by typing `ímport <ModuleName>`. You should not try to import it with the usual `pip install <StandardModule>`, because pip will generate an error and not find it.


## Give Dictionaries a Default Value
**use `collections.defaultdict` to avoid unneccesary errors when requesting a non-existent key**

Requesting a key from a dictionary which is not present generates a `KeyError` exception. While in some special cases this behaviour is actually very useful, it is way more often just simply annoying. [collections.defaultdict](https://pymotw.com/3/collections/defaultdict.html) is exactly solving this typical problem:

```python
from collections import defaultdict

colours = defaultdict(list)
colours['non_existing_colour'].append('#e75e40')  # will not throw an error

```
In the example above, `colours` will not be a normal dictionary, but a defaultdict object instead. Nevertheless, it behaves exactly as a built-in dictionary object without the annoying error when accessing non-existent keys.
