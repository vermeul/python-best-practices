# Code Layout

## Indentation

**Use 4 spaces for indentation. Do not use tabs.**

Mixing tabs and spaces leads to weird indentation errors, as tabs cannot easily be recognized by many editors. Therefore, tabs should be completely banned from your code. Most modern editors insert 4 spaces whenever you hit the tab key, which is a good compromise in terms of readability. However, evaluating the «correct» number of spaces for indentation often leads to relgious debates...

If you use the Vim editor, use these settings into your `.vimrc` file to insert 4 spaces when hitting the TAB key:

```
set tabstop=4
set expandtab
set shiftwidth=4
set shiftround
```

## Bracketing

The [PEP 8 style guide](https://www.python.org/dev/peps/pep-0008/) suggests a lot of variants how to use brackets.

Aligned with opening delimiter:

```python
def function_name(var_one, var_two,
                  var_three, var_four)
```

Function signature is more indented than the next following line:

```python
def long_function_name(
        var_one, var_two,
        var_three, var_four):
    pass
```

However, in terms of readability, these styles are not very optimal. I suggest to adopt the [K&R style](https://en.wikipedia.org/wiki/Indentation_style#Brace_placement_in_compound_statements) which adds more readability and can be used for functions, lists or dictionaries (or anything that uses brackets):


<strong>

```python
def long_function_name(
    var_one, var_two,
    var_three, var_four
):
    print("{} {} {} {}".format(
            var_four,
            var_three,
            var_two,
            var_one,
        )
    )
    
    my_dict = {
        "a": var_one,
        "b": var_two,
        "c": var_three,
        "d": var_four,
    }
        
```

</strong>

## Automatic Code Formatting

There are many automatic code formatters out there. Here are a few to try out (most of them work ok, but do not expect anything magic):

* Online: https://codebeautify.org/python-formatter-beautifier
* Online: https://www.cleancss.com/python-beautify/
* Black, the uncompromising code formatter: https://black.readthedocs.io/en/stable/index.html

