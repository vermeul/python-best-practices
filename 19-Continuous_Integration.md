# Continuous Integration

How to use GitLab and CI in a typical Python project that regularly should test your project, package it and send it to pypi.org

## getting started

### install tox and pyenv

**pyenv** is a Python version manager. Unlike virtualenv, which handles package versions, it handles various python versions without creating a mess.

**tox** lets you run python tests against various Python versions

* install [pyenv](https://github.com/pyenv/pyenv#homebrew-on-macos):
   * `brew update`
   * `brew install pyenv`
   * `brew upgrade pyenv`
* install the python versions you like to use, e.g.
   * `pyenv install --list`   # show all available python versions
   * `pyenv install 3.7.4`
   * `pyenv install 3.5.7`
   * `pyenv install 2.7.12'`
   * `pyenv versions`         # list all installed python versions
* install tox: `pip install tox`

### setting up pyenv

* in your project folder, specify the python you want your project to test against
* `pyenv local 3.7.4 3.5.7 2.7.12`      # Python version 3.7.4 will be used by default
* this will create a file in your directory: `.python-version`


### setting up tox

* in your project folder, create a `tox.ini` file with the following content:

```
[tox]
envlist = py37, py35, flake8

[testenv]
commands = py.test tests
deps = pytest

[testenv:flake8]
commands = flake8 tests
deps = flake8
skip_install = True
skipsdist = True
```

* It will run the tests using python 3.7 and python 3.5. It will also do a flake8 check aka [pep 8](https://pep8.readthedocs.io).
* run `tox` in your project folder to execute the tests manually


### setting up the Continuous Integration

* create a `.gitlab-ci.yaml` file in the root directory of your project (see content below)
* example below will install the latest python 3.7 version on your CI server as well as **tox** and **twine** (utility to upload packages to pypi.org)
* instead of using cleartext password, it uses an environment variable `$PYPI_PASSWORD`
* add this file to the project and push it to the gitlab server
* on the gitlab server, check if YAML file has the correct syntax
* define the password environment variable: `Settings --> CI/CD --> Variables` with Key: `PYPI_PASSWORD` and Value: (your pypi.org password)
* as soon as you commit on the master branch, the CI will start running
* the upload to `test.pypi.org` and `pypi.org` will not be done automatically (because we told it not to)
* you invoke the upload by a simple click: `CI / CD --> Pipelines` in the stage **pypi_upload** you will find two jobs: **pypi_test** and **pypi_prod**
* you can watch online what happens (and why it failed or succeeded)

```
stages:
    - style
    - tests
    - pypi_upload

before_script:
    - pyenv local $(latest_python 3.7)
    - pip install -U pip setuptools
    - pip install tox
    - pip install twine

tests:
    script:
        - pyenv local $(latest_python 3.7)
        - tox -r -vv -e py37
    stage: tests
    allow_failure: true

style:
    script:
        - pyenv local $(latest_python 3.7)
        - tox -r -vv -e flake8
    allow_failure: true
    stage: style

pypi_test:
    script:
        - pyenv local $(latest_python 3.7)
        - python setup.py sdist
        - twine upload -u your_username_on_pypi_org -p $PYPI_PASSWORD --repository-url https://test.pypi.org/legacy/ dist/*
    when: manual
    stage: pypi_upload

pypi_prod:
    script:
        - pyenv local $(latest_python 3.7)
        - python setup.py sdist
        - twine upload -u your_username_on_pypi_org -p $PYPI_PASSWORD dist/*
    when: manual
    stage: pypi_upload

```