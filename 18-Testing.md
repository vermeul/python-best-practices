# Testing

* start with pytest [https://pytest.org/en/latest/](https://pytest.org/en/latest/)
* for Jupyter notebook testing, read this [blog](https://www.blog.pythonlibrary.org/2018/10/16/testing-jupyter-notebooks/)

**only run a single test**

```bash
$ pytest tests/test_module.py::test_current_work
```

**run test that match a pattern**

```bash
$ pytest tests/ -k "MyClass and not weird_test"
$ pytest tests/ -k "test_current_work"
```

## Fixtures

* create a file named `conftest.py` within the `tests` folder
* add one or many fixtures as shown below
* set the `scope` to either `function`, `class`, `module`, `package` or `session` (see https://pytest.org/en/latest/how-to/fixtures.html#fixture-scopes)
* typically, database connections or other slow operations are `scope=session` to save time

```python
import pytest
import os
import mysql.connector

@pytest.fixture(scope="session")
def mysql_conn():
    db_host = os.getenv("DB_HOST")
    db_name = os.getenv("DB_NAME")
    db_user = os.getenv("DB_USER")
    db_password = os.getenv("DB_PASSWORD")

    mydb = mysql.connector.connect(
        host=db_host,
        user=db_user,
        password=db_password,
        database=db_name,
    )

    yield mydb

    mydb.close()
```

And then use fixture in your testing module(s), using the `mysql_conn` fixture as the first parameter:

```python
import pytest

def test_select_something(mysql_conn):
    mycursor = mysql_conn.cursor()
    mycursor.execute("SELECT * FROM customers")
    myresult = mycursor.fetchall()
    ...
```

