# Profiling

Whenever you think your code runs slow: do not try to optimise before you actually *measured* your code – known as profiling.

## the quick and easy way: line-profiler

Detailed documentation: https://github.com/pyutils/line_profiler

```
pip install line-profiler
```

Then simply add `@profile` decorators around the methods you think they are running slow. If you have no idea which method is slow, start from the top, run the profiler and then profile the method that sticks out next and so on:

```
@profile
def my_slow_method():
    do_this()
    do_that()
    do_something_else()
```

Without actively importing the profile decorator, your script would not run. But you can call your script with the `kernprof` command in front:

```
krenprof -vl my_slow_script.py
```

This will load the `@profile` decorator and execute it before the method is run. After the program exits (also due to an error) it will produce a nicely formatted table with every line of code of the decorated methods/functions and the amount of time (in percent of total time and seconds). The times are not 100% accurate, but it quickly gives you an impression on which line of the code the execution time is spend.

Now you can start optimising your code. Make sure you have written tests before you start your changes to make sure you are not introducing new errors. Then run the code again and measure it again. Do not spend too much time on minor (e.g. <10%) optimisations, unless you really need to. 
