# I/O

## always use the `with` statement to open a file

Until Python 2.5, the usual way to open a file and write something into it was like this:

```python
fh = open("say_hi.txt", "w")
print("hi", file=fh)
print("ho", file=fh)
not_allowed = 1/0     # throws an error!

fh.close()
```

the last line is never called and the file is never closed. Hence, the whole content is lost too, because the content is still in a memory buffer and not written to disk yet. To avoid this, *always* use the `with` statement introduced with Python 2.5:

```python
with open("say_hi.txt", "w") as fh:
    print("hi", file=fh)
    print("ho", file=fh)
    not_allowed = 1/0     # still creates an error, but now the content is already saved!
```

This is all fine, unless you start to write or read files that are anything else than ASCII. It is not safe to assume that everyone is using UTF-8 as their default locale (check `import locale; locale.getpreferredencoding()`). Instead, make the **encoding explicit** and add `encoding="utf-8"`

<strong>

```python
with open("say_hi.txt", "w", encoding="utf-8") as fh:
    print("I ❤︎ ♚ and ♛", file=fh)
    not_allowed = 1/0     # still creates an error, but now the content is already saved!
```
</strong>

## Open multiple files at the same time

If you need to open more than one file at the same time, you can do so by separating the `open` statements with a comma. For better readability, use a backslash `\` to split up a long line into two lines:

<strong>

```python
with open("/path/to/some/file/you/want/to/read", "r", encoding="utf-8") as file_1, \
     open("/path/to/some/file/being/written", "w", encoding="utf-8") as file_2:
     
    file_2.write(file_1.read())
```
</strong>

## Various ways to open a file

Modes available when opening a file:

- `rt` read a text file (the default)
- `r+` *read and write* a file
- `rb` read a *binary file*
- `rb+` *read and write* a binary file
- `a` append to a file
- `w` write to a file (overwrite, if exists)
- `wb` write binary file

Read the [documentation](https://docs.python.org/3/library/functions.html#open) for more detailed information.


## Safely delete files

<strong>

```python
import os
try:
    os.remove('some_file.tmp')
except OSError:
    pass
```
</strong>

## Redirect STDOUT to a file

This is how you would do it:

```python
import sys
original = sys.stdout
sys.stdout = open('redirect.txt', 'w')
print('This is your redirected text:')
print(text)
sys.stdout = original

print('This string goes to stdout, NOT the file!')
```

The only problem is: when you forget to put back the original `stdout` to `sys.stdout`, you will continue to write to the file until your program ends. It easy to get it wrong anyway. To avoid that, use the `@contextmanager` from [contextlib](https://pymotw.com/3/contextlib/). The `yield` statement is used to indicate that we actually are creating an iterator. If anything fails, we will still close the file and put back the standard output to its original place:

<strong>

```python
import sys
from contextlib import contextmanager

@contextmanager
def redirect_stdout(file, mode, encoding='utf-8'):
    oldstdtout = sys.stdout
    try:
        fileobj = open(file, mode, encoding=encoding)
        sys.stdout = fileobj
        yield fileobj
    except RuntimeError as err:
        print("    ERROR:", err)
    finally:
        fileobj.close()
        sys.stdout = oldstdtout
        
with redirect_stdout("redirect.txt", "w", encoding="utf-8") as f:
    print("This goes into a file, not to STDOUT!")
    
print("This will indeed print to STDOUT again.")
```
</strong>
