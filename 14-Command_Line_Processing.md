# Command Line Processing

* for many utility programs the command line is the primary interface
* so make it flexible
* and convenient
* and mnemonic
* and predictable
* standardize on a uniform command-line structure and use it consistently

## Meta-options

**Standardize your meta-options**

* `--usage` print a concise usage line
* `--help` print the `--usage` line, followed by a one-line summary of each option
* `--version` print the program's version number
* `--man` print complete documentation

Use the shorter names (e.g. `-u`, `-h`, `-v`, `-m`) not for meta-options (since they are not used very frequently) for your other options that your program needs.


## consider using the click module

**do not try to implement your own argument-parsing module**

The built-in `argparse` module is ok for basic arguments, but as your command-line tool grows and gets more complex, you might to switch to the great click module. It uses decorators as wrappers around the function that is exposed to the command line. These allow quite sophisticated **automatic type-checking of the input values** as well as **interactive shell**. This means, your actual function is freed from «infrastructure code», i.e. code that that only checks for correct input but otherwise does not contribute to the function.

```
import click

@click.command()
@click.option(
	'-y', '--year', 
	prompt=True,
	type=click.IntRange(2014, 2050),
)
@click.option(
	'-o','--out-folder', 
	prompt=True, 
	type=click.Path(exists=True),
)
@click.option(
	'--password',
	prompt=True, 
	confirmation_prompt=True, 
	hide_input=True,
)
def my_function(year, out_folder, password):
  print(year)
  print(out_folder)
  print(password)

if __name__ == '__main__':
   my_function()
   
```

## 