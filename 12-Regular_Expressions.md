# 12 Regular Expressions

## Do not use `re.match`, always use `re.search` instead

This regular expression below **does not matching anything**:

```python
import re
line = "Cats are smarter than dogs"
re.match(r'dogs', line)
```

But this does:

<strong>

```python
import re
line = "Cats are smarter than dogs"
re.search(r'dogs', line)
```

</strong>

The difference between `re.match()` and `re.search()` is that `re.match()` behaves as if every regex pattern is prepended with `^`:

```python
re.match(r'dogs', line)
re.search(r'^dogs', line) # same as above
```

Anyone accustomed to Perl, grep, or sed regular expression matching is mislead by `re.match()`. This method is error prone and and should be avoided.

There is actually a reason why `re.match` exists at all: it is **speed**. When no matching is possible, it takes a considerable amount [more time](https://stackoverflow.com/questions/29007197/why-have-re-match) for `re.search()` than `re.match()` to find this out. I am inclined to say: Python has an implementation problem here. I think `re.match()` should either be *deprecated*, or its current behaviour should be *fixed*. The speed gain in special cases should be implemented in the `re` module itself.


## always use `re.X` in long expressions

**Regular expressions are not easy to fix if your intention is not clear**

The re.X flag allows you to define regular expressions over multiple lines. More importantly, it allows you to add comments to every part, so the original intention is preserved. If something is wrong with the regular expression, such a commented regular expression is much easier to debug.

Who would like to debug this regular expression?

```python
regex = re.compile('^(?P<alias_alternative>(?P<requested_entity>experiment|collection)(\.(?P<attribute>\w+))?)(\s+(?i)AS\s+(?P<alias>\w+))?\s*$')
```

Split the regular expression on multiple lines and add comments. Of course, you need now to specify every blank space with `\s`, but this good practice anyway:
<strong>

```python
regex = re.compile(
    r"""^                                              # beginning of the string
        (?P<alias_alternative>                         # use first part as alias, if no alias is defined
          (?P<requested_entity>sample|object)          # string starts with sample or object
          (\.(?P<attribute>\w+))?                      # capture an optional .attribute
        )
        (                                              # capture an optional alias: entity.attribute AS alias
          \s+(?i)AS\s+                                 # ignore case of 'AS'
          (?P<alias>\w+)                               # capture the alias
        )?                                             # 
        \s*                                            # ignore any trailing whitespace
        $                                              # end of string
    """, re.X
)
```
</strong>


## Make use of **named capture groups**

A very common practice is to group elements in a regular expression:

```python
import re

url = '/some/url/our_first_parameter/our_second_parameter'
match = re.search(r'^/some/url/((.*?)/(.*?))$', url)
match.groups()

# returns
('our_first_parameter/our_second_parameter',
 'our_first_parameter',
 'our_second_parameter')
```

However, this leads to the problem that the parameters fetched are positional.  If you have nested group captures, you have to count the number of the opening round brackets `(` to get the position of every parameter right. And if you decide to remove a grouping later, you will have to check every position again.


Instead, you would rather give your groups a name so you can easily rearrange your groupings without having to worry about their positions. And we also make use of the `re.X` flag again.
<strong>

```python
import re

url = '/some/url/our_first_parameter/our_second_parameter'
match = re.search(r'''
    ^
    /some/url/                # the beginning of our url
    (?P<the_whole_thing>
        (?P<param1>.*?)
        /
        (?P<param2>.*?)
    )
    $''', url, flags=re.X)
match.groupdict()

# returns
{'the_whole_thing': 'our_first_parameter/our_second_parameter',
 'param1': 'our_first_parameter',
 'param2': 'our_second_parameter'}
```
</strong>

This leads to much more robust regular expressions.

### Group capture in substitutions or back-references
The group captures can be accessed in substitutions or in back-references, like this:
<strong>

```
\g<the_name_of_the_captured_group>
```
**Example**

```python
import re

url = '/some/url/our_first_parameter/our_second_parameter'
new_url = re.sub(r'''
    ^
    /some/url/
    (?P<the_whole_thing>
        (?P<param1>.*?)
        /
        (?P<param2>.*?)
    )
    $''',
    r'/some/other/url/\g<param2>/\g<param1>',
    url, flags=re.X)
    
new_url   #  /some/other/url/our_second_parameter/our_first_parameter
```
</strong>

## `re.split` – split string with a regular expression

The example above can be rewritten by using the traditional `strip()` function to remove leading and trailing spaces, and the `re.split` function to separate attribute and alias:

```python
input_string = input_string.strip()
attribute, alias = re.split(r'\s+AS\s+', input_string, flags=re.IGNORECASE)
```

However, if no alias was defined, this will throw the following error:

```python
ValueError: not enough values to unpack (expected 2, got 1)
```

This is very unfortunate, as we often encounter real-life problems, where a split might only return one value instead of two. To solve this, you can use this trick:

<strong>

```python
input_string = input_string.strip()
attribute, *alias = re.split(r'\s+AS\s+', input_string, flags=re.IGNORECASE)
alias = alias[0] if alias else attribute
```
</strong>

In this case, our alias will be the same as the attribute, if it was not explicitly defined.

## repeated captures: use the `regex` module.

The usual `re` does not support repeated captures, it always only returns the last capture. Use the `regex` module instead.

In the example below, we would like to extract all keys and values after `literals:

```python
import regex
import re

conf = """configMapGenerator:
  - name: api-env
    behavior: merge
    literals:
      - ADMIN_EMAIL=chucknorris@roundhouse.gov
      - SERVICE_HOSTNAME=...
      - ALLOWED_HOSTS=localhost,...
      - LOGSTASH_HOST=...
      - DEPLOYMENT_NAME=staging
  - name: frontend-env
    behavior: merge
    literals:
      - NEXT_PUBLIC_IMAGES_DOMAINS=localhost
"""

reg = regex.compile(
    r"""\s+literals\:\n
    (?:
        \s+-\s+
        (?P<key>.*?)
        \s*=\s*
        (?P<value>.*?)
        \n
    )+
    """, re.X
)

for m in reg.finditer(conf):
    print(m.capturesdict())

{'key': ['ADMIN_EMAIL', 'SERVICE_HOSTNAME', 'ALLOWED_HOSTS', 'LOGSTASH_HOST', 'DEPLOYMENT_NAME'], 'value': ['chucknorris@roundhouse.gov', '...', 'localhost,...', '...', 'staging']}
{'key': ['NEXT_PUBLIC_IMAGES_DOMAINS'], 'value': ['localhost']}
```
