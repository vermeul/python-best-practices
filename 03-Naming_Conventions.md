# Naming Conventions

* naming conventions are closely related to layout conventions
* only slightly less contentious
* find a consistent style of naming variables
* not just syntactically consistent
* but grammatically consistent as well


## Use\_underscores\_for\_identifiers

* doNotUseInterCaps
* underscores correspond better to the default natural language word separator (i.e. space)
* InterCaps (a.k.a. CamelCase) is harder to read and might conflict with all-caps CONSTANTS

## Distinguish constructs by case

* use lowercase for functions, methods and variable names
* use Mixed_Case for **class names** (if non avoidable, you can use CamelCase here)
* use UPPERCASE for constants

## Abbr idents by pref

* Nt by rmvng vwls
* it's acceptable to keep the last consonant
* especially with plural suffixes (e.g. identifiers --> idents)

## avoid ambigous abbreviations

* val = temp * dev
* valley? value? valid?
* temperature? temporary? template?
* deviation? device? develop?
 

## avoid synonyms if you mean the same:

* avoid a collection of methods that all display something on the screen:
   * show_prompt() 
   * display_alert()
   * present_result()
   * render_plot()
   * output_state()

* use the exactly same verb if these methods all display something on the screen, rather than synonyms:
   * display_prompt() 
   * display_alert()
   * display_result()
   * display_plot()
   * display_state()

* on the other hand, use distinct and non-synonymous verbs to make the difference clear:
   * print_prompt()    # output to terminal
   * display_alert()   # output to window manager / GUI
   * render_plot()     # output to visualization pane

* commonly abused synonyms include:
   * "show" vs "display" vs "present" vs "output" vs "render" 
   * "make" vs "create" vs "build" vs "generate"
   * "key" vs "identifier" vs "ID" vs "name"
   * "node" vs "item" vs "element" vs "entry"
   * "field" vs "member" vs "attribute" vs "slot"
   * "customer" vs "client" vs "user"
   * "colour" vs "color" vs "hue"

## select names from the problem domain, not the solution space

* typical solution-space names:
   * node
   * item
   * element
   * object
   * index
   * key
   * data
   * values
   * record
   * descriptor
   * list
   * tree
* treat such words as warning flags in your code

* Examples:
   * next_record() vs. next_gene_sample()
   * get_key() vs. get_message_ID()
   * read_file() vs. read_protein_samples()

* Choose names from the domain vocabulary of the problem you are solving
* Avoid names that relate only to the constructs you are using to solve it


## use grammatical templates to form identifiers

**for class names**

* Abstract noun: `Role`
* noun\_noun: `Role_Assignment` or `RoleAssignment`
* noun\_verb\_noun: `Dataset_Download_Queue`
* noun\_adjective: `Dataset_Deletable`


**for functions and methods**

* verb\_noun: `get_name`
* verb\_noun_preposition: `get_devices_for`
* verb\_noun\_participe: `execute_code_using`
* verb\_adjective\_noun: `delete_previous_task`


**for variables**

* noun: `node`, `source`, `destination`
* adjective\_noun: `running_total`, `data_structure`

## make arrays and sets plural, dictionaries singular

* dictionaries are usually one-to-one mappings of single values
* arrays and sets are usually collection of multiple values
* `events = []`
* `method_for = {}`

```
for event in events:
	method_for[event.type]()
```

## use `is`, `has`, `can` for booleans variables

* boolean variables are more self-documenting if they form a yes/no question
* `is_prime`
* `process_has_started`
* `can_overwrite`


## prefix `_for_internal_use_only` methods and functions

* Python has no easy way to prevent users from using an internal method
* instead, the underscore-prefix is used as a convention: `_compute_this()`
* Tools like Jupyter or IPython will follow this convention by not listing these internal methods when hitting the TAB for completion 