# Subroutines

## Always call functions with named arguments, not positional

Assume you have this function:
```python
def concat_names(first, last):
    print(first, last)
   
# later in the code 
concat_names("Frank", "Zappa")    # prints Frank Zappa
```

Now, you suddenly decide to rewrite the function to handle middle names:
```python
def concat_names(first='', middle='', last=''):
    if len(middle) is not 0:
        middle = middle[0] + '. '
    print(first+' ' + middle + last)
   
# later in the code, you still use it like this
concat_names("Frank", "Zappa")    # now suddenly prints: Frank Z.
```
If the function (or method) you are calling has more than one parameter, it is easy to get the positions wrong. Therefore, always use named parameters instead of positional arguments to make the code more robust:

```python
concat_names(first="Frank", last="Zappa", middle="Vincent")
```


## Never use an empty list `[]` as default parameter value, always use `None` instead.

You want to write this:
```python
def append_to(element, to=[]):
    to.append(element)
    return to
    
my_list_1 = append_to(1)   # we expect: [1]
my_list_2 = append_to(2)   # we expect: [2]. BOOM!
```

Unfortunately, this is what you get:
```python
my_list_1 = [1]
my_list_2 = [1,2]
```

The problem is, Pythons *default arguments are only evaluated once*, and not every time the function is called. This is not intuitive and definitely works differently in other languages.

**What you should write instead**
```python
def append_to(element, to=None):
    if to is None:
        to = []
    to.append(element)
    return to
```

It is not very elegant and adds unecessary infrastructure code into your function, instead of being just a method signature.

See also: https://docs.python-guide.org/writing/gotchas/


## Use an empty list or a dictionary to implement a state variable

An exception of the rule above is when you need to implement a state variable. A state variable is a permanent store of a value the first time a method or function gets executed. Here is an example for a password store which can only be executed by an inner method. This can be useful if you need the password later for reconnect to a server, without giving the possibility to easily get the cleartext password:

```
import inspect

class PW():
    def get_password_via_internal_method(self, *args, **kwargs):
        return self.password(*args, **kwargs)

    def password(self, password=None, pstore={} ):
        if password is not None:
            pstore['password'] = password
        else:
            if inspect.stack()[1][3] == 'get_password_via_internal_method':
                return pstore.get('password')
            else:
                raise Exception("Not allowed!")
                
# later
pw = PW()
pw.password('very_secret')
pw.get_password_via_internal_method()   # returns the password
pw.password() # will throw an Exception
```


## Use docstrings to comment your code

* Classes, functions and methods should have doctrings
* it's the easiest way to make a program self-documenting
* use tripple quotation marks (""" or ''') to start and end docstrings

```python
def complex(real=0.0, imag=0.0):
    """Form a complex number.

    Keyword arguments:
    real -- the real part (default 0.0)
    imag -- the imaginary part (default 0.0)
    
        this is an indented line
            even more so
    """
    if imag == 0.0 and real == 0.0:
        return complex_zero
    ...
```