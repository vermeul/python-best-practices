# Loop and List processing

## List comprehensions

In many cases, you don't need a classic for loop to walk through an iterable, you can use a list comprehension instead:

<strong>

```python
odd_numbers_squared = [x**2 for x in range(1,100) if x%2]
```
</strong>

But be aware, you can't put an `else` the end:

```python
odds_squared_evens_cubic = [x**2 for x in range(1,100) if x%2 else x**3]
                                                              ^
SyntaxError: invalid syntax
```

You have to **reorder the syntax**:

<strong>

```python
odds_squared_evens_cubic = [x**2 if %2 else 0 for x in range(1,100)]
```
</strong>

And just in case you wanted to always use this syntax:

```python
odd_numbers_squared = [x**2 if %2 for x in range(1,100)]
                       ^
SyntaxError: expected 'else' after 'if' expression
```



Make sure you break your list comprehension over several lines, as it easily gets unreadable:

```python
vowels_separated_and_quoted = [f"«{character}»" for character in "this is a text" if character in ['a','e','i','o','u']]
```
<strong>

```python
vowels_separated_and_quoted = [
    f"«{character}»"
    for character in "this is a text" 
    if character in ['a','e','i','o','u']
]
```
</strong>
 

## Double List comprehensions
**Use line breaks when writing double list comprehensions**

Consider a data structure like this:

```python
keys_recipients = [
    { "uids":
        [
            {"email": "one@domain"},
            {"email": "two@domain"},
            {"email": "three@domain"},
        ]
    }
]
```

To extract all the email addresses in the example above, one would intuitively do this:

```python
[ uid["email"] for uid in key["uids"] for key in keys_recipients ]
# NameError: name 'key' is not defined
```

The reason for the error gets a bit clearer when written over several lines (recommended!):

```python
[
    uid["email"]
    for uid in key["uids"]      # inner loop: key is not yet defined -> NameError
    for key in keys_recipients  # outer loop: key is only defined here
]
# NameError: name 'key' is not defined
```

For these not so very obvious reasons, the looping is the other way round:

```python
[
    uid["email"]
    for key in keys_recipients  # outer loop
    for uid in key["uids"]      # inner loop
]
# ["one@domain", "two@domain", "three@domain"]
```

As a user points it out in Stackoverflow: _List comprehension syntax is not one of Python's shining points._


## Controlling a for loop
**use `break` and `continue` to stop or to continue with the next iteration**

The usual way to exit a loop is the `break` statement. It will stop the for-loop altogether:

<strong>

```python
for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print( n, 'equals', x, '*', n/x)
            break
```
</strong>

```
4 equals 2 * 2.0
6 equals 2 * 3.0
8 equals 2 * 4.0
9 equals 3 * 3.0
```

The counterpart of the `break` statement is the `continue` statement: it will jump to the next iteration instead:
<strong>

```python
for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print( n, 'equals', x, '*', n/x)
            continue
```
</strong>

```
4 equals 2 * 2.0
6 equals 2 * 3.0
6 equals 3 * 2.0
8 equals 2 * 4.0
8 equals 4 * 2.0
9 equals 3 * 3.0
```

## Check if for-loop run through normally
**Use `for ... else` to make sure your for-loop did not encounter a `break` statement.**

In certain cases a `for ... else` construct can produce elegant solutions in control structures:

<strong>

```python
for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print( n, 'equals', x, '*', n/x)
            break
    else:
        # loop did not encounter a break
        # i.e. it fell through without finding a factor
        print(n, 'is a prime number')
```
</strong>

```
2 is a prime number
3 is a prime number
4 equals 2 * 2.0
5 is a prime number
6 equals 2 * 3.0
7 is a prime number
8 equals 2 * 4.0
9 equals 3 * 3.0
```

See http://book.pythontips.com/en/latest/for_-_else.html

## List Generation
**Use `map` to generate lists out of existing lists**

An *imperative approach* to transform the numbers 1..10 into floating point numbers is the following:

```
floats = []
for i in range(1, 11):
	floats.append(float(i))
```
A *functional approach* looks like this:

<strong>

```python
def make_floats(x):
    return float(x)
    
floats = map(make_floats, range(1,11))
list(floats)
```
</strong>

```
[1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]
```

**Note:** The `map` function returns a `map` object which we can iterate through or transform into a `list`.


## List Selections
**Use `filter` instead of a `for` loop**

To filter certain values from a list, a typical *imperative approach* would involve a for loop:

```python
interesting = []
for i in range(1,11):
	if x in [5,6]:
		pass
	else:
		interesting.append(i)

# interesting: [1, 2, 3, 4, 7, 8, 9, 10]
```

The `filter` function takes another function as its first parameter. That function then must return a `True` boolean value for those inputs that should be kept:

<strong>

```python
def not_5_nor_6(x):
    if x in [5,6]:
        return False
    else:
        return True
    
interesting = filter(not_5_nor_6, range(1,11))
```
</strong>

**Note:** In the second approach, we do not receive a `list` object. Instead, we receive a `filter` object which we can iterate through:

```
for item in interesting:
	print(item)
```

## Process and condense Lists
**Use `reduce` to process a list into a single value**

To add all numbers from 1 to 100, an *imperative approach* would be:

```python
total = 0
for i in range(1,101):
    total += i
```

A *functional approach* is preferrable, especially when the calculation gets more complex. We will use `reduce` from `functools`:

<strong>

```python
from functools import reduce
import operator

total = reduce(operator.add, range(1, 101))
```
</strong>

The first argument of `reduce` must be a function which takes two arguments at a time. The above approach can be rewritten as:

<strong>

```python
from functools import reduce

def add(x, y):
	return x + y
	
total = reduce(add, range(1, 101))
```
</strong>

The `reduce` function accepts both lists and sequences as its second parameter.


## Functional vs. imperative approach
**Use a functional approach when appropriate**

We have seen many *functional approaches* when dealing with lists. But why should we use a functional approach at all? For various (not so obvious!) reasons:

* because the **function** itself is outside the for-loop, it can be easily **reused** and even put into an external module
* the function is executed in a **lazy manner**, which means it will not be executed until it needs to. Often, this leads to a performance boost.
* it is more **declarative**: we declare what we would like to do. The implementation details of the **how** is being separated, which is a good thing.

## Lambda
**don’t use lambda.**

`lambda` is probably the weakest concept in Python. It is an attempt to provide an ad-hoc anonymous function:

```python
from functools import reduce

reduce(lambda x, y: x * y, range(1,11))
```

Unfortunately, `lambda` has so many drawbacks and limitations, that it is not recommended to use all. In short, `lambda` is the incompetent brother of `def`. Even Guido van Rossum wanted it to remove from Python 3. Instead of `lambda`, use an explicit function:

<strong>

```python
from functools import reduce

def times(x, y):
    return x * y

reduce(times, range(1,11))
```
</strong>
