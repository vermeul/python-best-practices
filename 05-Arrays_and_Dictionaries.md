# Arrays and Dictionaries

## Combining arrays

Combining arrays is actually very easy:

<strong>

```python
arr_one = [1,2,3]
arr_two = [4,5,6]
arr_one_and_two = arr_one + arr_two    # is now [1,2,3,4,5,6]
```
</strong>


## Merging arrays
**Use `set` to make its elements unique**

If you want to avoid duplicates, you need to convert it to a **set** first and convert it back to a **list**:

<strong>

```python
a = [1, 2, 3, 4, 5, 6]
b = [5, 6, 7, 8]
c = list( set(a + b) )
>>> [1,2,3,4,5,6,7,8]
```
</strong>


## Removing certain elements from an array
**Make use of Pythons list comprehension facility.**

Unfortunately, removing elements from an array is not as easy as combining two arrays:

```python
arr_one_and_two = [1,2,3,4,5,6]
arr_one = [1,2,3]
arr_two = arr_one_and_two - arr_one     # ERROR
```

Instead, you need to do this:

<strong>

```python
arr_one = [1,2,3]
arr_one_and_two = [1,2,3,4,5,6]
arr_two = [elem for elem in arr_one_and_two if elem not in arr1]
```
</strong>

If do not have to keep a copy of the original array, you can **remove** the unwanted elements instead:
<strong>

```python
arr_one = [1,2,3]
arr_one_and_two = [1,2,3,4,5,6]
for elem in arr_one:
    arr_one_and_two.remove(elem)
    
# arr_one_and_two is now: [4,5,6]
```
</strong>

## Filtering arrays
**make use of the `filter` function**

If you would like to take out certain elements of an array, you might consider a for-loop using a `yield` keyword (for lazy evaluation):

```
def get_numbers():
    for x in numbers:
        if x%3 == 0:
            yield x
            
list(get_numbers())

>>> [3, 6, 9, 12, 15, 18]
```
Unless you have very complex filter calculations, this is not very ideal. A better way is to use **list comprehensions**:

```
numbers = list(range(1,20))
list(x for x in numbers if x%3 == 0)

>>> [3, 6, 9, 12, 15, 18]
```
List comprehensions are short in writing, but not always easy to comprehend.

See also [06-Loops_and_Lists](06-Loops_and_Lists.md) for more sophisticated ways to process data.


```
numbers = list(range(1,20))
list(filter(lambda x: x%3 == 0, numbers))
```

## Merging two dictionaries

**Use the new Python 3.5 syntax if you can: `{**dict1, **dict2}`**

For some not very obvious reasons, combining two dictionaries is not as easy as combining two arrays. Here are some unsuccessful, naïve approaches:

```python
dict1 = { 'a': 1 }
dict2 = { 'b': 2 }
dict_combined = dict1 + dict2      # ERROR
dict_combined = {dict1 + dict2}    # ERROR
dict_combined = dict(dict1, dict2) # ERROR
```

In Python 3.4 and lower, as well as Python 2 (which you should not use anyway) you *need to define a helper function* instead:

<strong>

```python
def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

dict_combined = merge_two_dicts(dict1, dict2)
```
</strong>

To be honest, this is just bullshit. Basic operations like merging two dictionaries should be much easier. Fortunately, in **Python 3.5** or greater (which you should be using) they included a very easy solution to this common problem:

<strong>

```python
dict_combined = {**dict1, **dict2}
```
</strong>

Note that this even works when `dict1` and `dict2` share a common
key. In this case the values of `dict1` will be overwritten by `dict2`.
This is not the case for function calls, i.e. the following code
will give an error:

```python
dict1 = {'a': 1, 'b': 2}
dict2 = {'b': 3, 'c': 4}
def f(a, b, c): pass
f(**dict1, **dict2)
```

If you need backward compatibility, use the copy and update method described above. In all other cases, use the new syntax, it is cleaner and more concise.


## Avoid Errors when accessing values in arrays, dictionaries and objects

In Python, various errors might occur when you try to access a non-existing value from a data structure: 

* Arrays (Lists): **IndexError**
* Dictionaries: **KeyError**
* Objects: **AttributeError**

We do not want to use the annoying `try ... except` structure all the time. We rather need a safe way to access values. Unfortunately, Python demands a different approach for every object:

**Array**

Accessing an element in an array which might be missing is particularly nasty:

```python
a = [9,8,7,6,5,4]
a[7]                                       # IndexError
a[7] if len(a) > 7 else 'nothing here'     # OK
```

**Dictionary**

Dictionaries offer a generic `get` method to safely access an item in a dictionary:

```python
dict = {}
dict['not_here]                            # KeyError
dict.get('not_here', 'alternative value')  # OK
```

**Object**

Objects do not offer any specialized method to access internal attributes in a safe way. Instead, Python demands the generic `getattr` method. Make sure you offer an alternative value, otherwise an AttributeError will be thrown:

```python
my_object = SomeClass()
getattr(my_object, 'non_existing_attribute_name')          # throws AttributeError
getattr(my_object, 'maybe_existing_attribute_name', None)  # returns the value of the attribute (or None, if it does not exist)
```

## Module variables

**use `global` to set a global value from within a function.**

Let's assume you have a global variable which you want to define later in a subroutine:

```python
my_global_var = 0

def set_globvar_to_value(val):
    my_global_var = val

def print_globvar():
    print(my_global_var)

print_globvar()    # prints 0, as expected
set_globvar_to_value(666)
print_globvar()     # BONG! still prints 0, not 666!
```

The fundamental problem of the Python language is its lack of proper scope and the absence of a variable declaration statement. Whenever you assign a variable to a value, Python defaults to **function scope**.

Because of the fact that global variables (global within a module) are read-accessible from within a function but **not directly modifiable**, they are considered to be *evil*. However, in my opinion, global variables have their reason to exist and are perfectly fine.

So if you need to **modify a global variable**, you  need to add the keyword `global` to tell Python that you do not want to define a new function-scope variable (which is the default) but to modify the existing global variable:

<strong>

```python
my_global_var = 0

def set_globvar_to_value(val):
    global globvar  # we would like to access the global variable
    globvar = val

def print_globvar():
    print(globvar)

print_globvar()    # prints 0, as expected
set_globvar_to_value(666)
print_globvar()     # YES! Now prints 666!
```
</strong>

On Stackoverflow, this question has almost 2600 upvotes and more than 3600 upvotes for the solution: 
https://stackoverflow.com/questions/423379/using-global-variables-in-a-function
